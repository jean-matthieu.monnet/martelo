# Martelo

An R script to prepare the tree map of a marteloscope from field measures and optional ALS (airborne laser scanning) data.

Provided files:

- data/marteloscope.Rmd: the main R script, provided in Rmarkdown,
- data/functions.R: some R functions called by the script
- data/field_measures.xlsx: field measures from the Saint Agnan marteloscope (forest belongs to the French Forest Office, measures provided by INRAE-LESSEM).
- data/lidar.laz: airborne laser scanning data acquired other the marteloscope. Copyright INRAE-LESSEM.

Files provided under the GPL 3 license